<?php require 'config/Config.php'; ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>LICEU.management</title>

        <link href="<?= $base_url; ?>assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= $base_url; ?>assets/css/core.css" rel="stylesheet" type="text/css" />
        <link href="<?= $base_url; ?>assets/css/components.css" rel="stylesheet" type="text/css" />
        <link href="<?= $base_url; ?>assets/css/icons.css" rel="stylesheet" type="text/css" />
        <link href="<?= $base_url; ?>assets/css/pages.css" rel="stylesheet" type="text/css" />
        <link href="<?= $base_url; ?>assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="<?= $base_url; ?>assets/css/styles.css" rel="stylesheet" type="text/css" />
        <link href="<?= $base_url; ?>assets/plugins/hopscotch/css/hopscotch.min.css" rel="stylesheet" type="text/css" />

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?= $base_url; ?>assets/js/modernizr.min.js"></script>

    </head>

    <body class="fixed-left">

        <!-- Begin page -->
        <div id="wrapper">

            <!-- Top Bar Start -->
            <div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <a href="<?= $base_url; ?>" class="logo"><span>LICEU</span></a>
                    </div>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left waves-effect waves-light">
                                    <i class="md md-menu"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                            <ul class="nav navbar-nav navbar-right pull-right">
                                <li class="dropdown top-menu-item-xs">
                                    <a href="#" data-target="#" class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                                        <i class="icon-bell"></i> <span class="badge badge-xs badge-danger">1</span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-lg">
                                        <li class="notifi-title"><span class="label label-default pull-right">1 nouă</span>Notificări</li>
                                        <li class="list-group slimscroll-noti notification-list">
                                            

                                            <!-- list item-->
                                            <a href="javascript:void(0);" class="list-group-item">
                                                <div class="media">
                                                    <div class="pull-left p-r-10">
                                                        <em class="fa fa-bell-o noti-custom"></em>
                                                    </div>
                                                    <div class="media-body">
                                                        <h5 class="media-heading">Test titlu</h5>
                                                        <p class="m-0">
                                                            Descriere notificare
                                                        </p>
                                                    </div>
                                                </div>
                                            </a>

                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" class="list-group-item text-right">
                                                <small class="font-600">Vezi toate</small>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="hidden-xs">
                                    <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                                </li>
                                
                                <li class="dropdown top-menu-item-xs">
                                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true"><img src="assets/images/avatar-1.jpg" alt="user-img" class="img-circle"> </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="javascript:void(0)"><i class="ti-user m-r-10 text-custom"></i> Profilul meu</a></li>
                                        <li><a href="javascript:void(0)"><i class="ti-settings m-r-10 text-custom"></i> Setări</a></li>
                                        <li class="divider"></li>
                                        <li><a href="javascript:void(0)"><i class="ti-power-off m-r-10 text-danger"></i> Ieșire</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>

            <div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>

                            <li class="text-muted menu-title">Navigare</li>


                            <li class="has_sub"><a href="/orar.php?time=agendaDay" class="waves-effect"><i class="ti-shine"></i> <span> Azi </span>  </a></li>
                            <li class="has_sub"><a href="/orar.php?time=agendaWeek" class="waves-effect"><i class="ti-layers-alt"></i> <span> Săptămâna aceasta </span>  </a></li>
                            <li class="has_sub"><a href="/cataloage.php" class="waves-effect"><i class="ti-agenda"></i> <span> Cataloage </span>  </a></li>
                            <li class="has_sub"><a href="#" class="waves-effect"><i class="ti-pulse"></i> <span> Rapoarte </span>  </a></li>

                            
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
			<div class="content-page">
				<!-- Start content -->
				<div class="content">
					<div class="container">
<?php require './template/header.php'; ?>

<div class="row">
    <div class="col-md-3">
        <div class="panel panel-color panel-primary" id="clasa-item">
            <div class="panel-heading">
                <h3 class="panel-title">Clasa 5B</h3>
            </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <strong>Elevi: 16</strong>
                    </li>
                    <li class="list-group-item">
                        Dirigine: <strong>Iulia Vornic</strong>
                    </li>
                    <li class="list-group-item">
                        <a href="/catalog.php" class="btn btn-default">Vezi catalog</a>
                        <a href="#" class="btn btn-success">Vezi lista elevi</a>
                    </li>
                </ul>

        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-color panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Clasa 6A</h3>
            </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <strong>Elevi: 22</strong>
                    </li>
                    <li class="list-group-item">
                        Dirigine: <strong id="diriginte">Beatrice Julia</strong>
                    </li>
                    <li class="list-group-item">
                        <a href="/catalog.php" class="btn btn-default">Vezi catalog</a>
                        <a href="#" class="btn btn-success">Vezi lista elevi</a>
                    </li>
                </ul>

        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-color panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Clasa 7A</h3>
            </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <strong>Elevi: 23</strong>
                    </li>
                    <li class="list-group-item">
                        Dirigine: <strong>Maria Roibu</strong>
                    </li>
                    <li class="list-group-item">
                        <a href="/catalog.php" class="btn btn-default">Vezi catalog</a>
                        <a href="#" class="btn btn-success">Vezi lista elevi</a>
                    </li>
                </ul>
        </div>
    </div>
    <div class="col-md-3">
        <div class="panel panel-color panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Clasa 9C</h3>
            </div>
                <ul class="list-group">
                    <li class="list-group-item">
                        <strong>Elevi: 27</strong>
                    </li>
                    <li class="list-group-item">
                        Dirigine: <strong>Ana Ivanov</strong>
                    </li>
                    <li class="list-group-item">
                        <a href="/catalog.php" class="btn btn-default" id="info-clasa">Vezi catalog</a>
                        <a href="#" class="btn btn-success">Vezi lista elevi</a>
                    </li>
                </ul>
        </div>
    </div>
</div>


<?php require './template/footer.php'; ?>

<script>
    
    $(document).ready(function () {
        var placementRight = 'right';
        var placementLeft = 'left';

        // Define the tour!
        var tour = {
            id: "my-intro-index3",
            steps: [
                {
                    target: "clasa-item",
                    title: "Clasa",
                    content: "Aici sunt indicate clasele cu care aveți perechi",
                    placement: placementRight,
                    yOffset: 10
                },
                {
                    target: 'diriginte',
                    title: "Dirigintele clasei",
                    placement: placementRight,
                    zindex: 999
                },
                {
                    target: 'info-clasa',
                    title: "Catalogul clase",
                    content: "Catalog săptămânal al clasei",
                    placement: 'bottom',
                    zindex: 999
                }
            ],
            showPrevButton: true
        };

        // Start the tour!
        hopscotch.startTour(tour);
    });
</script>
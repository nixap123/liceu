<?php require './template/header.php'; ?>


<div class="card-box">
    <div id="calendar"></div>
</div>


<?php require './template/footer.php'; ?>

<!--calendar css-->
<link href="assets/plugins/fullcalendar/css/fullcalendar.min.css" rel="stylesheet" />

<!-- calendar js -->
<script src="assets/plugins/moment/moment.js"></script>
<script src='assets/plugins/fullcalendar/js/fullcalendar.min.js'></script>
<script src="assets/pages/jquery.fullcalendar.js"></script>

<script>
    
    <?php if (isset($_GET['time'])) { ?>
    $('#calendar .fc-<?= $_GET['time'] ?>-button').click();
    <?php } ?>
    
    $(document).ready(function () {
        var placementRight = 'right';
        var placementLeft = 'left';

        // Define the tour!
        var tour = {
            id: "my-intro-index2",
            steps: [
                {
                    target: ".fc-today-button",
                    title: "Navigarea",
                    content: "Schimbă ziua/săptămâna sau vezi orarul pe azi",
                    placement: placementRight,
                    yOffset: 10
                },
                {
                    target: '.fc-month-button',
                    title: "Variante navigare",
                    content: "Alegeți calendarul zilnic/săptămânal/lunar",
                    placement: placementLeft,
                    zindex: 999
                },
                {
                    target: '.fc-time-grid-event:first',
                    title: "Oră",
                    content: "Aici aveți ora și clasa, click pe item și deschideți catalogul acestei clase",
                    placement: 'bottom',
                    zindex: 999
                }
            ],
            showPrevButton: true
        };

        // Start the tour!
        hopscotch.startTour(tour);
    });
</script>
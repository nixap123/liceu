<?php require './template/header.php'; ?>

<div class="row">
    <div class="col-sm-12">

        <h4 class="page-title">Catalog</h4>
        <ol class="breadcrumb">
            <li>
                <a href="#">Liceu</a>
            </li>
            <li>
                <a href="#">Clasa 5B</a>
            </li>
            <li class="active">
                Catalog
            </li>
        </ol>
    </div>
</div>


<div class="row">
    <div class="col-sm-12 col-md-9">
        <div class="card-box">
            <h4 class="m-t-0 header-title">
                <b>Clasa 5B (3 elevi) - <u id="catalog-clasa">Matematica</u></b>

                <div class="pull-right">
                    <small id="schimba-saptamana">Schimbă săptămâna: </small>
                    <a href="#" class="btn btn-xs btn-success btn-animated"><i class="ti-angle-double-left"></i></a>
                    <a href="#" class="btn btn-xs btn-success btn-animated"><i class="ti-angle-double-right"></i></a>
                </div>

                <p>&nbsp;</p>
            </h4>

            <div class="clearfix"></div>

            <div class="table-responsive">
                <table id="mainTable" class="table table-bordered m-b-0">
                    <thead>
                        <tr>
                            <th class="nota-item">#</th>
                            <th>Nume, prenume</th>
                            <th class="nota-item">22.01</th>
                            <th class="nota-item">24.01</th>
                            <th class="nota-item">26.01</th>
                            <th class="nota-item">27.01</th>
                            <th class="nota-item">30.01</th>
                            <th class="nota-item">01.02</th>
                            <th class="nota-item">03.02</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="nota-item">1</td>
                            <td><a href="/">Marian Ion</a></td>
                            <td class="nota-item">6</td>
                            <td class="nota-item">a</td>
                            <td class="nota-item">7</td>
                            <td class="nota-item">9</td>
                            <td class="nota-item">5</td>
                            <td class="nota-item">-</td>
                            <td class="nota-item">-</td>
                        </tr>
                        <tr>
                            <td class="nota-item">2</td>
                            <td><a href="/">Oloieri Vadim</a></td>
                            <td class="nota-item">9</td>
                            <td class="nota-item">5</td>
                            <td class="nota-item">6</td>
                            <td class="nota-item">8</td>
                            <td class="nota-item">a</td>
                            <td class="nota-item">-</td>
                            <td class="nota-item">-</td>
                        </tr>
                        <tr>
                            <td class="nota-item">3</td>
                            <td><a href="/">Pavaloi Alexandra</a></td>
                            <td class="nota-item">a</td>
                            <td class="nota-item">6</td>
                            <td class="nota-item">8</td>
                            <td class="nota-item">7</td>
                            <td class="nota-item">9</td>
                            <td class="nota-item">-</td>
                            <td class="nota-item">-</td>
                        </tr>
                        <tr>
                            <td class="nota-item">4</td>
                            <td><a href="/">Palamari Violin</a></td>
                            <td class="nota-item">3</td>
                            <td class="nota-item">4</td>
                            <td class="nota-item">7</td>
                            <td class="nota-item">5</td>
                            <td class="nota-item">9</td>
                            <td class="nota-item">-</td>
                            <td class="nota-item">-</td>
                        </tr>
                        <tr>
                            <td class="nota-item">5</td>
                            <td><a href="/">Palamari Aliona</a></td>
                            <td class="nota-item">9</td>
                            <td class="nota-item">9</td>
                            <td id="click-nota" class="nota-item">8</td>
                            <td class="nota-item">7</td>
                            <td class="nota-item">9</td>
                            <td class="nota-item">-</td>
                            <td class="nota-item">-</td>
                        </tr>
                        <tr>
                            <td class="nota-item">6</td>
                            <td><a href="/">Rudenco Sabrina</a></td>
                            <td class="nota-item">8</td>
                            <td class="nota-item">7</td>
                            <td class="nota-item">7</td>
                            <td class="nota-item">9</td>
                            <td class="nota-item">8</td>
                            <td class="nota-item">-</td>
                            <td class="nota-item">-</td>
                        </tr>
                        <tr>
                            <td class="nota-item">7</td>
                            <td><a href="/">Raducan Gheorghe</a></td>
                            <td class="nota-item">9</td>
                            <td class="nota-item">10</td>
                            <td class="nota-item">8</td>
                            <td class="nota-item">10</td>
                            <td class="nota-item">7</td>
                            <td class="nota-item">-</td>
                            <td class="nota-item">-</td>
                        </tr>
                        <tr>
                            <td class="nota-item">8</td>
                            <td><a href="/">Soltuz Vasile</a></td>
                            <td class="nota-item">6</td>
                            <td class="nota-item">5</td>
                            <td class="nota-item">7</td>
                            <td class="nota-item">7</td>
                            <td class="nota-item">6</td>
                            <td class="nota-item">-</td>
                            <td class="nota-item">-</td>
                        </tr>
                        <tr>
                            <td class="nota-item">9</td>
                            <td><a href="/">Ticalos Mihaela</a></td>
                            <td class="nota-item">8</td>
                            <td class="nota-item">8</td>
                            <td class="nota-item">8</td>
                            <td class="nota-item">9</td>
                            <td class="nota-item">7</td>
                            <td class="nota-item">-</td>
                            <td class="nota-item">-</td>
                        </tr>
                        <tr>
                            <td class="nota-item">10</td>
                            <td><a href="/">Teodorovici Dmitrii</a></td>
                            <td class="nota-item">a</td>
                            <td class="nota-item">5</td>
                            <td class="nota-item">5</td>
                            <td class="nota-item">8</td>
                            <td class="nota-item">6</td>
                            <td class="nota-item">-</td>
                            <td class="nota-item">-</td>
                        </tr>
                        <tr>
                            <td class="nota-item">11</td>
                            <td><a href="/">Traian Băsescu</a></td>
                            <td class="nota-item">9</td>
                            <td class="nota-item">8</td>
                            <td class="nota-item">8</td>
                            <td class="nota-item">10</td>
                            <td class="nota-item">9</td>
                            <td class="nota-item">-</td>
                            <td class="nota-item">-</td>
                        </tr>
                        <tr>
                            <td class="nota-item">12</td>
                            <td><a href="/">Tarasiuc Elena</a></td>
                            <td class="nota-item">10</td>
                            <td class="nota-item">6</td>
                            <td class="nota-item">7</td>
                            <td class="nota-item">8</td>
                            <td class="nota-item">10</td>
                            <td class="nota-item">-</td>
                            <td class="nota-item">-</td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div class="clearfix"></div>


        </div>
    </div>

    <div class="col-md-3">
        <div id="tema-clasei" class="panel panel-border panel-custom">
            <div class="panel-heading">
                <h3 class="panel-title">Tema clasei</h3>
            </div>
            <div class="panel-body">
                <p>
                    Ecuații de gradul doi
                </p>
            </div>
        </div>
        <div id="tema-acasa" class="panel panel-border panel-danger">
            <div class="panel-heading">
                <h3 class="panel-title">Tema pentru acasă</h3>
            </div>
            <div class="panel-body">
                <p>
                    De rezolvat de la pagina 112, exercițiile 3,4 și 5
                </p>
            </div>
        </div>
    </div>
</div>

<?php require './template/footer.php'; ?>


<link rel="stylesheet" href="assets/plugins/jquery-datatables-editable/datatables.css" />
<script src="assets/plugins/jquery-datatables-editable/jquery.dataTables.js"></script> 
<script src="assets/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="assets/plugins/tiny-editable/mindmup-editabletable.js"></script>
<script src="assets/plugins/tiny-editable/numeric-input-example.js"></script>


<script src="assets/pages/datatables.editable.init.js"></script>

<script>
    $('#mainTable').editableTableWidget().numericInputExample().find('td:first').focus();

</script>


<script>
    $(document).ready(function () {
        var placementRight = 'right';
        var placementLeft = 'left';

                // Define the tour!
                var tour = {
                    id: "my-intro",
                    steps: [
                        {
                            target: "catalog-clasa",
                            title: "Info clasa",
                            content: "Aici este indicată clasa, nr. de elevi și obiectul",
                            placement: placementRight,
                            yOffset: 10
                        },
                        {
                            target: 'schimba-saptamana',
                            title: "Navigare catalog",
                            content: "Click butonul din stânga veți merge cu o săptămână înapoi sau click buton dreapta, înainte",
                            placement: placementLeft,
                            zindex: 999
                        },
                        {
                            target: 'tema-clasei',
                            title: "Tema clase",
                            content: "Tema clasei în dependență de ziua selectată",
                            placement: placementLeft,
                            zindex: 999
                        },
                        {
                            target: 'tema-acasa',
                            title: "Tema pentru acasă",
                            content: "Tema pentru acasă din această zi",
                            placement: placementLeft,
                            zindex: 999
                        },
                        {
                            target: 'click-nota',
                            title: "Schimbă/adaugă notă",
                            content: "Click pe nota pentru a schimba sau adăuga",
                            placement: 'top',
                            zindex: 999
                        }
                    ],
                    showPrevButton: true
                };

        // Start the tour!
        hopscotch.startTour(tour);
    });
</script>
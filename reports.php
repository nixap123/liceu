<?php require './template/header.php'; ?>

<div class="card-box">
    
    <h1>Rapoarte</h1>

    <div class="card-box widget-inline">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <div class="widget-inline-box text-center">
                    <h3><i class="text-primary md md-add-shopping-cart"></i> <b data-plugin="counterup">8954</b></h3>
                    <h4 class="text-muted">Lifetime total sales</h4>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="widget-inline-box text-center">
                    <h3><i class="text-custom md md-timer"></i> <b data-plugin="counterup">7841</b></h3>
                    <h4 class="text-muted">Total lectii/ore</h4>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="widget-inline-box text-center">
                    <h3><i class="text-pink md md-account-child"></i> <b data-plugin="counterup">1459</b></h3>
                    <h4 class="text-muted">Total elevi</h4>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6">
                <div class="widget-inline-box text-center b-0">
                    <h3><i class="text-purple md md-visibility"></i> <b data-plugin="counterup">49</b></h3>
                    <h4 class="text-muted">Total invatatori</h4>
                </div>
            </div>
        </div>
    </div>

</div>

<?php require './template/footer.php'; ?>

<!--Chartist Chart-->
<script src="assets/plugins/chartist/js/chartist.min.js"></script>
<script src="assets/plugins/chartist/js/chartist-plugin-tooltip.min.js"></script>
<script src="assets/pages/jquery.chartist.init.js"></script>

<script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

<!-- jQuery  -->
<script src="assets/plugins/moment/moment.js"></script>

<!-- jQuery  -->
<script src="assets/plugins/waypoints/lib/jquery.waypoints.js"></script>
<script src="assets/plugins/counterup/jquery.counterup.min.js"></script>

<!-- jQuery  -->
<script src="assets/plugins/bootstrap-sweetalert/sweet-alert.min.js"></script>

<!-- skycons -->
<script src="assets/plugins/skyicons/skycons.min.js" type="text/javascript"></script>

<script src="assets/plugins/peity/jquery.peity.min.js"></script>

<script src="assets/pages/jquery.widgets.js"></script>

<!-- Todojs  -->
<script src="assets/pages/jquery.todo.js"></script>

<!-- chatjs  -->
<script src="assets/pages/jquery.chat.js"></script>

<!-- Knob -->
<script src="assets/plugins/jquery-knob/jquery.knob.js"></script>

<script src="assets/plugins/jquery-sparkline/jquery.sparkline.min.js"></script>

<script src="assets/js/jquery.core.js"></script>
<script src="assets/js/jquery.app.js"></script>


<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".knob").knob();
    });
</script>